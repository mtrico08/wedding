<?php 
require_once('includes/session.php'); 
require_once('includes/constants.php');
?>
<html>
<title>Spaide & Ricotta Wedding | Our Favorite Things</title>
<!-- NOTE, header includes an opened and unclosed DOM element -->
<?php 
	require_once('includes/header.php'); 
?>
	<div id="wrapper" class="">
		<div class="row">
			<div class="container center">
				<h1>Our Favorite Things</h1>
			</div>
			<div class="container two-cols row">
				<div class="col">
					<p>Autumn is festival season here, there is a lot to do, see, and visit. To help, we've listed our favorite eateries, bars, and sites. Don't just take our word, though, there are many famous restaurants and houses not on this list.</p>
					<p>Savannah's history and beauty is cause for its booming film industry including the current Lady and The Tramp filming at our own wedding venue! Savannah was also responsible for Forrest Gump, the founding of The Girl Scouts, the 2nd bloodiest battle of the Revolutionary War, a top 3 most haunted US cities (partly due to the yellow fever), and the 2nd largest St. Patty's parade...</p>
				</div>
				<div class="col">
					<ul>
						<?php 
							$categories = array_keys($favorites); 
							foreach($categories as $cat){
								echo '<li><a class="blue" href="#'.strtolower($cat).'">'.ucfirst($cat).'</a></li>';
							}
						?>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="container two-cols">
				<?php
					$zoom = 10;
					$size = '1000x150';
					$gmkey = 'AIzaSyDBZIK1soCiFKxUIaTVBHvUntdY2sfwjjM';
				
					$gmapsurl = 'https://maps.googleapis.com/maps/api/staticmap?center=';
					//$gmapsurl .='Georgia+State+Railroad+Museum,Savannah,GA';
					$gmapsurl .='Courtyard+by+Marriott+Savannah+Downtown,Savannah,GA';
					$gmapsurl .='&zoom='.$zoom;
					$gmapsurl .='&size='.$size;
					$gmapsurl .='&maptype=roadmap';
					foreach ($favorites as $category=>$places){
						echo '<div class="col" style="float: left;"><h2 id="'.strtolower($category).'" class="venue blue">'.ucfirst($category).'</h2>';
						foreach($places as $place){
							if($place['status']==true&&$place['hide']==false){
								$color = isset($place['color'])? $place['color'] : 'blue';
								$string = '<div class="row">';
								$string .= 	'<h3>'.$place['name'].'</h3>';
								$string .= 	isset($place['address'])? '<h4><strong>Address: </strong>'.$place['address'].'</h4>' : '';
								$string .= 	isset($place['phone'])? '<h4><strong>Phone: </strong>'.$place['phone'].'</h4>' : '';
								$string .= 	isset($place['link'])? '<h4><strong>Link: </strong><a href="'.$place['link'].'">'.$place['link'].'</a></h4>' : '';
								$string .= 	isset($place['notes'])? '<p>'.$place['notes'].'</p>' : '';
								$string .= '</div>';
								echo $string;
								$gmapsurl .='&markers=color:'.$color.'%7Clabel:'.strtoupper(substr($place['name'],0,1)).'%7C'.$place['coords'];
							}
						}
						echo '</div>';
						echo '<div class="col" style="float: none;"></div>';
					}
					$gmapsurl .='&key='.$gmkey;
				?>
			</div>
		</div>
		<div class="row">
			<div class="container">
				<h2>Not good enough for you!?</h2>
				<p><em>Some other great options not listed: Alligator Soul, Prohibition Restaurant, Rancho Alegre, Circa 1875</em></p>
			</div>
		</div>
		<div id="map" class="row one-cols">
				<img class="full" src="<?php echo $gmapsurl; ?>"/>
		</div>
	</div>
<?php require_once('includes/footer.php'); ?>
</html>