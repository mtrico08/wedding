<?php 
require_once('includes/session.php'); 
require_once('includes/constants.php');
?>
<html>
<title>Spaide & Ricotta Wedding</title>
<!-- NOTE, header includes an opened and unclosed DOM element -->
<?php 
	require_once('includes/header.php'); 
	
	$username = isset($_SESSION['user']['contact'])? $_SESSION['user']['contact'] : 'YOU!';
	$addressG = isset($_SESSION['user']['address'])? $_SESSION['user']['address'] : '';
	$cityG = isset($_SESSION['user']['city'])? $_SESSION['user']['city'] : '';
	$stateG = isset($_SESSION['user']['state'])? $_SESSION['user']['state'] : '';
	$zipG = isset($_SESSION['user']['zip'])? $_SESSION['user']['zip'] : '';
	$foodG = isset($_SESSION['user']['food'])? $_SESSION['user']['food'] : '';
	$rsvp = isset($_SESSION['user']['rsvp'])? $_SESSION['user']['rsvp'] : 'false';
	$row = isset($_SESSION['user']['row'])? $_SESSION['user']['row'] : 'false';
	
	$states = [					"AK"=>"Alaska",
								"AZ"=>"Arizona",
								"AR"=>"Arkansas",
								"CA"=>"California",
								"CO"=>"Colorado",
								"CT"=>"Connecticut",
								"DE"=>"Delaware",
								"DC"=>"District Of Columbia",
								"FL"=>"Florida",
								"GA"=>"Georgia",
								"HI"=>"Hawaii",
								"ID"=>"Idaho",
								"IL"=>"Illinois",
								"IN"=>"Indiana",
								"IA"=>"Iowa",
								"KS"=>"Kansas",
								"KY"=>"Kentucky",
								"LA"=>"Louisiana",
								"ME"=>"Maine",
								"MD"=>"Maryland",
								"MA"=>"Massachusetts",
								"MI"=>"Michigan",
								"MN"=>"Minnesota",
								"MS"=>"Mississippi",
								"MO"=>"Missouri",
								"MT"=>"Montana",
								"NE"=>"Nebraska",
								"NV"=>"Nevada",
								"NH"=>"New Hampshire",
								"NJ"=>"New Jersey",
								"NM"=>"New Mexico",
								"NY"=>"New York",
								"NC"=>"North Carolina",
								"ND"=>"North Dakota",
								"OH"=>"Ohio",
								"OK"=>"Oklahoma",
								"OR"=>"Oregon",
								"PA"=>"Pennsylvania",
								"RI"=>"Rhode Island",
								"SC"=>"South Carolina",
								"SD"=>"South Dakota",
								"TN"=>"Tennessee",
								"TX"=>"Texas",
								"UT"=>"Utah",
								"VT"=>"Vermont",
								"VA"=>"Virginia",
								"WA"=>"Washington",
								"WV"=>"West Virginia",
								"WI"=>"Wisconsin",
								"WY"=>"Wyoming",
			];
	
?>
	<div id="who" class="lightbox">
		<div class="popup">
			<h2>You're invited!</h1>
			<h3>Enter your email,<br/>or click the link we've provided.</h3>
			<form method="post" action="/includes/setemail.php">
				<input name="email" id="email" type="email" placeholder="email" required autocomplete="email" value=""/>
				<input type="submit" class="button" href="">Let's Go</a>
			</form>
			<div id="errors">
				<?php echo (isset($_SESSION['error']))? $_SESSION['error'] : ''; ?>
			</div>
		</div>
	</div>
	<div id="wrapper" class="">
		<div class="row">
			<div class="container center">
				<h1>It's a Match!</h1>
				<div id="animation" class="row two-cols">
					<div class="col"><img src="/img/mike.jpg"/></div>
					<div class="col"><img src="/img/becky.jpg"/></div>
				</div>
			</div>
		</div>
		<div class="row three-cols">
			<div class="col"><img src="/img/engagement3.jpg"/></div>
			<div class="center col container">
				<h2>And you're invited!</h2>
				<div id="rsvp">
					<button data-rsvp="yes">Heck Yeah!</button>
					<button data-rsvp="no">Sorry, Charlie, I/we will be missing out!</button>
					<form id="rsvp-form" class="hidden">
						<div class="switch-field">
						  <div class="switch-title"><?php echo $username; ?></div>
							<?php
								// Invisible field allows us to retrieve which users are RSVP'ing so we can retrieve their specific keys
								if($rsvp == 'yes'){
									$str = '<input type="radio" id="switch_left_'.$row.'" checked name="switch_'.$row.'" value="yes"/>';
									$str .=  '<label for="switch_left_'.$row.'">Will Be There!</label>';
									$str .=  '<input type="radio" id="switch_right_'.$row.'" name="switch_'.$row.'" value="no" />';
									$str .=  '<label for="switch_right_'.$row.'">Cannot Make It</label>';
								} elseif($rsvp == 'no'){
									$str = '<input type="radio" id="switch_left_'.$row.'" name="switch_'.$row.'" value="yes"/>';
									$str .=  '<label for="switch_left_'.$row.'">Will Be There!</label>';
									$str .=  '<input type="radio" id="switch_right_'.$row.'" checked name="switch_'.$row.'" value="no" />';
									$str .=  '<label for="switch_right_'.$row.'">Cannot Make It</label>';
								} else {
									$str = '<input type="radio" id="switch_left_'.$row.'" name="switch_'.$row.'" value="yes"/>';
									$str .=  '<label for="switch_left_'.$row.'">Will Be There!</label>';
									$str .=  '<input type="radio" id="switch_right_'.$row.'" name="switch_'.$row.'" value="no" />';
									$str .=  '<label for="switch_right_'.$row.'">Cannot Make It</label>';		
								}
								echo $str;
							?>
						</div>
						<div>
						<?php if (isset($foodG)){
									echo '<input type="checkbox" name="food_'.$row.'" value="tacos">Tacos, Por Favor</input>';
									echo '<input type="checkbox" name="food_'.$row.'" value="pizza">Pizza, Dude</input>';
								} else {
									echo '<input type="checkbox" name="food_'.$row.'" value="tacos">Tacos, Por Favor</input>';
									echo '<input type="checkbox" name="food_'.$row.'" value="pizza">Pizza, Dude</input>';
								}
						?>
						</div>
						<div><h3>Choose as many foods as you'd like, we won't hold you to it!  Just need a head count.</h3></div>
						<div>
						<span class="plus">
						<?php
								$pluses = isset($_SESSION['group'])? $_SESSION['group'] : null;
								if(isset($pluses)&&$pluses!=null&&count($pluses)>1){
									$count = count($pluses)-1;
									$string = 'Along with '.$count.' guests';
								} else {
									//$string = 'Need a plus one?  Please blame Mike and let us know by phone or email!';
									$string = '';
								}
								echo $string;
							?>
						</span>
						</div>
						<?php 
							if(isset($pluses)&&$pluses!=null&&count($pluses)>1){
								$ppl=[];
								$str = '';
								foreach($pluses as $plus){
									$i = $plus['row'];
									$ppl[]=$i;
									$rsvp2 = isset($plus['rsvp'])? $plus['rsvp'] : 'false';
									//print($username.' '); print($username.' ');
									if($plus['contact']==$username){
										continue;
									}
									$str .= '<div class="switch-field"><div class="switch-title">';
									$str .= $plus['contact'];
									$str .= '</div>';
									if($rsvp2 == 'yes'){
										$str .= '<input type="radio" id="switch_left_'.$i.'" checked name="switch_'.$i.'" value="yes"/>';
										$str .=  '<label for="switch_left_'.$i.'">Will Be There!</label>';
										$str .=  '<input type="radio" id="switch_right_'.$i.'" name="switch_'.$i.'" value="no" />';
										$str .=  '<label for="switch_right_'.$i.'">Cannot Make It</label>';
									} elseif($rsvp2 == 'no'){
										$str .= '<input type="radio" id="switch_left_'.$i.'" name="switch_'.$i.'" value="yes"/>';
										$str .=  '<label for="switch_left_'.$i.'">Will Be There!</label>';
										$str .=  '<input type="radio" id="switch_right_'.$i.'" checked name="switch_'.$i.'" value="no" />';
										$str .=  '<label for="switch_right_'.$i.'">Cannot Make It</label>';
									} else {
										$str .= '<input type="radio" id="switch_left_'.$i.'" name="switch_'.$i.'" value="yes"/>';
										$str .=  '<label for="switch_left_'.$i.'">Will Be There!</label>';
										$str .=  '<input type="radio" id="switch_right_'.$i.'" name="switch_'.$i.'" value="no" />';
										$str .=  '<label for="switch_right_'.$i.'">Cannot Make It</label>';		
									}
									$str .= '</div><div><input type="checkbox" name="food_'.$i.'" value="tacos">Tacos, Por Favor</input>';
									$str .= '<input type="checkbox" name="food_'.$i.'" value="pizza">Pizza, Dude</input></div>';
									$str .=  '<input class="hidden" type="text" id="contact_'.$i.'" name="contact_'.$i.'" value="'.$plus['contact'].'" />';
								}
								$str .=  '<input class="hidden" type="text" id="ppl" name="ppl" value="'.implode(',',$ppl).'" />';
								echo $str;
							}
						?>
						<input type="submit" class="button" value="WooHoo!">
					</form>
					<div id="spinner" class="spinner hidden">
					</div>
					<div id="message">
					</div>
					<div id="addresses" class="hidden">
						<h3>Please provide your mailing address, so we can keep in touch!</h3>
						<form id="address-form" class="one-cols align-left">
							<label for="address" class="col">Address</label>
							<input name="address" type="input" class="col" id="address" placeholder="152 Main St." autocomplete="shipping street-address" value="<?php echo $addressG; ?>">
							<label for="city" class="col">City</label>
							<input name="city" type="input" class="col" id="city" placeholder="" autocomplete="shipping locality" value="<?php echo $cityG; ?>">
							<label for="state" class="col">State</label>
							<select id="state" class="custom-select w-100 col" name="state" autocomplete="shipping region">
                      			<option value="">Select State</option>
								<?php
									foreach($states as $key=>$stater){
										$selected = ($key == $stateG)? 'selected' : '';
										echo '<option value="'.$key.'" selected="'.$selected.'">'.$stater.'</option>';
									}
								?>
							</select>
							<label for="zip" class="col">Zip</label>
							<input name="zip" type="input" class="form-control col" id="zip" placeholder="" autocomplete="shipping postal-code" value="<?php echo $zipG; ?>">
							<label for="phone" class="col">Phone</label>
							<input name="phone" type="input" class="form-control col" id="zip" placeholder="" autocomplete="tel" value="<?php echo $zipG; ?>">
							<input class="hidden" type="text" id="contact" name="contact" value="<?php echo $row; ?>" />
							<input type="submit" class="button col" value="Submit">
						</form>
					</div>
				</div>
			</div>
			<div class="col"><img src="/img/engagement2.jpg"/></div>
		</div>
		<div class="row one-cols">
			<div class="container col">
				<h2 class="venue <?php echo $details['venue']['color']; ?>">Time & Place</h2>
				<div class="row two-cols">
					<div class="col">
						<h3 class="center">
							<div><?php echo $details['venue']['name']; ?></div>
							<div class="address"><?php echo $details['venue']['address']; ?></div>
							<div class="link"><a href="<?php echo $details['venue']['link']; ?>"><?php echo $details['venue']['link']; ?></a></div>
						</h3>
					</div>
					<div class="col">
						<h3 class="center">
							<div class="date">Saturday, October 20th, 2018</div>
						</h3>
						<div class="date center fancy">
							Ceremony: 5:30pm<br/>
							Cocktail Hour: 5:35pm<br/>
							Reception: 6:30pm<br/>
							After Party: 10pm-ish
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="hotels" class="row">
			<div class="container">
				<h2>Hotel Accommodations</h2>
				<ul>
				<?php
				foreach($details as $key=>$place){
					if(null===$place['hide']||$place['hide']!==true){
						$rating = '';
						$status = ($place['status']!==null&&$place['status']!==false)? 'active' : 'inactive';
						for($i=0;$i<$place['rating'];$i++){
							$rating .= '<span class="fa fa-star"></span>';
						}
						if($key == 'venue'){
							continue;
						}
						$string = '<li class="location '.$status.'">';
						$string .= '<div class="name '.$place['color'].'">'.$place['name'].'</div>';
						$string .= '<div class="address">'.$place['address'].'</div>';
						$string .= '<div class="tel" href="tel:1-'.$place['phone'].'">'.$place['phone'].'</div><br/>';
						$string .= '<div class="rating center">'.$rating.'</div>';
						$string .= '<div class="price center">'.$place['price'].'</div><br/>';
						$string .= '<div class="link"><a href="'.$place['link'].'">'.$place['link'].'</a></div>';
						$string .= '<br/><div class="notes"><em>'.$place['notes'].'</em></div>';
						$string .= '</li>';
						echo $string;
					}
				}
				?>
				</ul>
				<br/>
				<hr>
				<div class="center">We recommend booking your hotel and flights early.  October is festival season, so travel will be busy here.<br/><br/>p.s.  A list of events in the area will be provided to maximize your stay!<br/><br/>p.p.s Lyft and Uber are readily available and very inexpensive in the area (we usually pay $5-$10/ride).</div>
				<hr>
			</div>
		</div>
		<div class="row one-cols">
			<div class="container col">
				<h2 class="attire">Attire</h2>
				<div class="row two-cols">
					<div class="col">
						<h3>
							Attire:&nbsp;&nbsp; Cocktail <span class="smaller">(No, spilling on yourself doesn't qualify as cocktail attire)</span>
						</h3>
					</div>
					<div class="col">
						<h3>
							Theme:&nbsp;&nbsp; Low Country Speakeasy (tasteful, please)<br/>
						</h3>
					</div>
				</div>
				<div class="row two-cols">
					<div class="col">
						<h4>Men</h4>
						<ul>
							<li>Have some fun with the theme, a bit of humor is encouraged.</li>
							<li>Avoid standard navy blue suits.  Slacks jeans and a sportcoat is fine.</li>
							<li>Bow-ties, suspenders, 3-piece... all that hipster stuff will fit in.</li>
						</ul>
					</div>
					<div class="col">
						<h4>Women</h4>
						<ul>
							<li>Have some fun with the theme.  Outfit ideas, below.</li>
							<li>Avoid high-heels, this is an outdoor venue.</li>
							<li>Flapper style dresses are encouraged.</li>
						</ul>
					</div>
				</div>
				<!--div class="row center three-cols"-->
				<div class="row center two-cols">
					<div class="col"><img src="/img/mens-clothing.jpg"/></div>
					<!--div class="col"><img src="/img/clothing.jpg"/></div-->
					<div class="col"><img src="/img/womens-clothing.jpg"/></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="center">
				<h3>Gifts / Registry</h3>
				<div>
					Rather than a registry, we'd prefer gifts are made out to our HoneyFund to help us get started.  Thanks!<br/>
					<a href="https://www.honeyfund.com/wedding/ALottaSpicotta2018" class="link">Honey Fund: https://www.honeyfund.com/wedding/ALottaSpicotta2018</a>
				</div>
			</div>
		</div>
		<div id="map" class="row one-cols">
				<?php
					$zoom = 15;
					$size = '1000x150';
					$gmkey = 'AIzaSyDBZIK1soCiFKxUIaTVBHvUntdY2sfwjjM';
				
					$gmapsurl = 'https://maps.googleapis.com/maps/api/staticmap?center=';
					//$gmapsurl .='Georgia+State+Railroad+Museum,Savannah,GA';
					$gmapsurl .='Courtyard+by+Marriott+Savannah+Downtown,Savannah,GA';
					$gmapsurl .='&zoom='.$zoom;
					$gmapsurl .='&size='.$size;
					$gmapsurl .='&maptype=roadmap';
					foreach ($details as $place){
						if(null===$place['hide']||$place['hide']!==true){
							$gmapsurl .='&markers=color:'.$place['color'].'%7Clabel:'.strtoupper(substr($place['name'],0,1)).'%7C'.$place['coords'];
						}
					}
					$gmapsurl .='&key='.$gmkey;
				?>
				<img class="full" src="<?php echo $gmapsurl; ?>"/>
		</div>
		<div class="row">
			<div class="center">
				<h2>Don't forget to save the date!</h2>
			</div>
		</div>
	</div>
<?php require_once('includes/footer.php'); ?>
</html>
