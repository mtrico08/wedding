$(document).ready(function(){
	var guest = $('body').data('guest');
	if(typeof guest == 'undefined' || guest.length == 0){
		$('#who.lightbox').show();
	}
	var scrollTo = function() {
		location.hash = "#message";
	}
	$('#rsvp > button').click(function(){
		var rsvp = $(this).data('rsvp');
		$('#rsvp > button').hide();
		if(rsvp == 'yes'){
			$(this).parent().find('form#rsvp-form').show();
		} else {
			$('.switch-field :input[value="no"]').prop('checked', true);
			$('.plus').hide();
			saver();
		}
	});
	$('#rsvp form#rsvp-form').submit(function(event){
		event.preventDefault();
		saver();
	});
	$('#rsvp form#address-form').submit(function(event){
		event.preventDefault();
		storeaddress();
	});
	$(function() {
	  $('#animation div:first-child img').animate({
			left: '40%'
	  }, 750);
	  $('#animation div:last-child img').animate({
			right: '40%'
	  }, 750);
	});
	/* AJAX SUBMIT THE FORM */
	function saver(){
		$('#spinner').show();
		var data = {};
		data['email'] = $('body').data('guest');
		$('#rsvp form#rsvp-form input').each(function(){
			var type = $(this).attr('type');
			if(type=='radio'){
				if(!$(this).is(':checked')){
					return true;
				} else {
					//var key = $(this).parent().find('.switch-title').text();
					var key = $(this).attr('name');
					var value = $(this).attr('value');
					data[key] = value;
				}
			} else if(type=='checkbox') {
				if(!$(this).is(':checked')){
					return true;
				} else {
					var key = $(this).attr('name');
					var value = $(this).attr('value');
					if(typeof data[key] !== 'undefined' && data[key].length){
						data[key] += ','+value;
					} else {
						data[key] = value;
					}
				}
			} else {
				var key = $(this).attr('name');
				var value = $(this).attr('value');
				data[key] = value;
			}
		});
		$.post("includes/post.php", data, function(d){
			$('#rsvp #message').html(d).show();
			$('#rsvp #message').html(d).show();
			if(d.indexOf('Awesome') !== -1){
				$('#rsvp form#rsvp-form').hide();
				$('#rsvp #addresses').show();
			}
			$('#spinner').hide();
			scrollTo();
		});
	}
	/* AJAX SUBMIT THE FORM */
	function storeaddress(){
		$('#spinner').show();
		var data = {};
		data['email'] = $('body').data('guest');
		$('#rsvp form#address-form input').each(function(){
			var type = $(this).attr('type');
			var key = $(this).attr('name');
			var value = $(this).val();
			data[key] = value;
		});
		var state = $('#rsvp form#address-form #state').find(":selected").text();
		data['state']=state;
		$.post("includes/post_address.php", data, function(d){
			$('#rsvp #message').html(d).show();
			$('#rsvp #message').html(d).show();
			$('#spinner').hide();
			$('#rsvp #addresses').hide();
			scrollTo();
		});
	}
	/* AJAX SET THE EMAIL */
	function setmail(){
		data['email'] = $('body').data('guest');
		$.post("includes/setemail.php", data, function(d){
			if(d !== false){
				$('.popup').hide();
			} else {
				$('.popup #errors').html(d);
				$('.popup #errors').html(d);
			}
		});
	}
	/* WHEN RSVP NO BUTTON IS CLICKED, MUST POST AS RSVP NO */
});