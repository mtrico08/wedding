<?php
require_once('constants.php');
require_once('google/vendor/autoload.php');
require_once('google.php');
require_once('session.php'); 

if(isset($_POST)&&count($_POST)>0){
	$putData = [];
	if(isset($_POST['contact'])){
		$putData[$_POST['contact']] = $_POST;
		unset($putData[$_POST['contact']]['contact']);
		unset($putData[$_POST['contact']]['email']);
		unset($putData[$_POST['contact']]['undefined']);
	}
	storeAddress($putData);
} else {
	echo 'Bummer!  We\'ll miss you!<br/><strong>Made a mistake?</strong> Just refresh and re-start.';
}

// Stores the address information based on the "row" key within the postdata array
function storeAddress($postData){
	$google = new Guests();
	$addresses = $google->putAddresses($postData);
	if(!empty($addresses)){
		echo 'Excellent, all up-to-date. Nothing to do from here!';
	} else {
		echo 'Uh...  That didn\'t quite work out as planned.  That\'s okay, we\'ll sort it out later.';
	}	
}

?>