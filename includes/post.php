<?php
require_once('constants.php');
require_once('google/vendor/autoload.php');
require_once('google.php');
require_once('session.php'); 

if(isset($_POST)&&count($_POST)>0){
	$message = (!in_array('yes',$_POST))? 'Bummer!  We\'ll miss you!<br/><strong>Made a mistake?</strong> Just refresh and re-start.' : null;
	rsvp('invited@spaidericottawedding.com',$_POST,$message);
} else {
	echo 'Sorry, we didn\'t get any info in that submission. Please try again.';
}

function rsvp($sender,$postData,$message=null){
	$email = $postData['email'];
	$ppl = explode(',',$postData['ppl']);
	$putData = [];
	foreach($ppl as $p){
		if(isset($postData['switch_'.$p])){
			$putData[$p]['rsvp'] = $postData['switch_'.$p];
			$putData[$p]['food'] = isset($postData['food_'.$p])? $postData['food_'.$p] : 'pizza';
		}
	}
	$body = '';
	foreach($_POST as $k=>$p){
		$body .= $k.': '.$p.'<br/>';
	}
	$to = 'michael.ricotta@gmail.com';
	$subject = 'rsvp from '.$email;
				$headers = 'From: '.$sender."\r\n".
				  'X-Mailer: PHP/' . phpversion();
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	$store = storeRsvp($putData);
	if(false!==$store){
		mail($to,$subject,$body,$headers);
		$message = (isset($message)&&$message!==null)? $message : 'Awesome, we\'ve got you covered!<br/><br/>We\'ll keep this page updated with any new information leading up to the big day, so feel free to revisit this for future information.';
		echo $message;
		return true;
	} else {
		$message = 'Whoops, we ran into an error storing your RSVP. Call Mike or Becky, we\'ll rsvp you.';
		echo $message;
		return false;
	}
}

// Stores the RSVP information based on the "row" key within the postdata array
function storeRsvp($postData){
	$google = new Guests();
	$emails = $google->putGuests($postData);
	if(!empty($emails)){
		return true;
	} else {
		return false;
	}	
}

?>