<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/responsive.css">
</head>
<body data-guest="<?php echo (isset($_SESSION['user']['email']))? $_SESSION['user']['email'] : ''; ?>">
	<div class="wrapper">
		<header>
			<div id="header" class="row two-cols">
				<div class="container two-cols">
					<div id="nav" class="col">
						<ul>
							<!--li><a href="/our-story.php">Our Story</a></li-->
							<li><a href="/">Home</a></li>
							<li><a href="/our-favorite-things.php">Things To Do</a></li>
							<li><a href="/timeline.php">Timeline of Events</a></li>
						</ul>
					</div>
					<div id="details" class="col">
						<span id="date">10.20.18 @5:30pm</span>
						<span id="location">Savannah GA</span>
					</div>
				</div>			
			</div>
		</header>
	</div>