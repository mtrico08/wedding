<?php
require_once('constants.php');
require_once('google/vendor/autoload.php');
require_once('google.php');
require_once('session.php'); 

if(isset($_POST['email'])){
	$guest = findGuest(strtolower($_POST['email']));
	// lookup email on the spreadsheet... if it's there, then
	if($guest !== false){
		$_SESSION['user'] = $guest;
		$groupId = $guest['group num'];
		$_SESSION['group'] = findGroup($groupId);
		unset($_SESSION['error']);
		header('Location: '.DOMAIN);
	} else {
		$_SESSION['user'] = '';
		$_SESSION['error'] = 'Sorry, that email wasn\'t found.';
		header('Location: '.DOMAIN);
	}
} else {
	$_SESSION['user'] = '';
	$_SESSION['error'] = 'Sorry, that email wasn\'t found.';
	header('Location: '.DOMAIN);
}

function findGroup($groupId){
	$google = new Guests();
	$guests = $google->getGuests();
	$group = [];
	if(!empty($guests)){
		foreach($guests as $guest){
			if($guest['group num']==$groupId){
				$group[] = $guest;
			}
		}
		return $group;
	}
	return false;
}
	

// findGuest looks up all user data by specific email address
function findGuest($email){
	$google = new Guests();
	$guests = $google->getGuests();
	if(!empty($guests)){
		foreach($guests as $guest){
			if(in_array(strtolower($email),$guest)||(isset($guest['email'])&&strtolower($guest['email'])==strtolower($email))){
				return $guest;
			}
		}
	}
	return false;	
}

// getGuests returns all guests
function getGuests(){
	$google = new Guests();
	$emails = $google->getGuests();
	if(!empty($emails)){
		return true;
	} else {
		return false;
	}
}

?>
