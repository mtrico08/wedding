<?php
/** Required for Google API **/
require_once('constants.php');
require_once('google/vendor/autoload.php');

/** 
	Accepts 
**/

class Guests{
  protected $Google_API_KEY;
  protected $Google_PASSWORD;
  public $temp_array;
  public $guest_file;
  public $googleLibraryDir;
  public $googleSecretFile;
  public $googleAppName;
  public $googleScopes;
  public $service;
  public $_redirectURI;
  
  public function __construct(){
	/**		Google Sheets API ADMIN SETTINGS	OAuth Client		**/
	/**/	$this->Google_API_KEY = '310483828562-4e0uni79lduo2gggj10e0gu8g1kmvia0.apps.googleusercontent.com';
	/**/	$this->Google_PASSWORD = 'tIi1kjWAtlUehVaCQ3lJiWt8';
	/**/	$this->googleLibraryDir = 'google/';
	/**/	$this->googleSecretFile = $this->googleLibraryDir.'client_secret.json';
	/**/	//$this->googleSecretFile = 'C:/xampp/wedding/includes/google/client_secret.json';
	/**/	$this->guest_file = '1MKQNiNwJv8_sH1W0lAw0HH7KzRe8E8Qb4DDBYINE5Yc';
	/**/	$this->googleAppName = 'Wedding Guest List';
	/**/	$this->googleCredPath = $this->googleLibraryDir.'sheets.googleapis.com-php-quickstart.json';
	/**/	// If modifying these scopes, delete your previously saved credentials in .json file.
	/**/	$this->googleScopes = implode(' ', array(Google_Service_Sheets::SPREADSHEETS));
			//$this->_redirectURI = 'http://spaidericottawedding.com';
	/**/	// ^ AKA https://www.googleapis.com/auth/spreadsheets // Allows read/write access to sheets
	/**		END Google Sheets ADMIN SETTINGS						**/
	$this->temp_array = array();
	return true;
  }
  
  /** Get the API client and construct the service object.
  **/
  public function setGoogleAPI(){
	$client = self::getClient();
	$this->service = new Google_Service_Sheets($client);
	return TRUE;
  }
  
  /**
  /*	getGuests stores all guests to an array
  **/
  public function getGuests(){
	  $METHOD = 'GET';
	  $CALL = 'guests';
	  try{
		$results = self::runFunction($METHOD,$CALL);
	  } catch(\Exception $e) {
		print('Caught exception 1: '.$e->getMessage()."\n");
		return FALSE;
	  }
	  return $results;
  }

  /**
  /*	getEmails stores all guests to an array
  **/
  public function getEmails(){
	  $METHOD = 'GET';
	  $CALL = 'emails';
	  try{
		$results = self::runFunction($METHOD,$CALL);
	  } catch(\Exception $e) {
		print('Caught exception 1: '.$e->getMessage()."\n");
		return FALSE;
	  }
	  return $results;
  }

  /**
  /*	putGuests stores all RSVP data to the sheet
  **/
  public function putGuests($postData){
	  $METHOD = 'PUT';
	  $CALL = 'guests';
	  foreach($postData as $key=>$post){
		  try{
			$range = 'F'.$key.':G'.$key;
			$results = self::runFunction($METHOD,$CALL,$post,$range);
		  } catch(\Exception $e) {
			print('Caught exception 1: '.$e->getMessage()."\n");
			return FALSE;
		  }
	  }
	  return true;
  }

  /**
  /*	putGuests stores all RSVP data to the sheet
  **/
  public function putAddresses($postData){
	  $METHOD = 'PUT';
	  $CALL = 'addresses';
	  foreach($postData as $key=>$post){
		  try{
			$range = 'J'.$key.':N'.$key;
			$results = self::runFunction($METHOD,$CALL,$post,$range);
		  } catch(\Exception $e) {
			print('Caught exception 1: '.$e->getMessage()."\n");
			return FALSE;
		  }
	  }
	  return true;
  }
  
  /**
  **/
  public function runFunction($method,$call,$postData=NULL,$range=NULL){
		if($method == 'GET'){
			try{
				// Get All Data In Current Spreadsheet
				if($call=='emails'){
					// Unique value is in column C
					$range = 'I1:I';
				} elseif($call=='groups'){
					$headers = 'D1:D';
				} elseif($call=='guests'){
					$range = 'A1:Z200';
				} else {
					$range = 'A1:A';
				}
				self::setGoogleAPI();
				$response = $this->service->spreadsheets_values->get($this->guest_file, 'Guest Contacts!'.$range);
				$headers = $response->getValues()[0];
				$values = $response->getValues();
				unset($values[0]);
				$values = array_values($values);
				$results = [];
				foreach($values as $key=>$items){
					for($i=0; $i<count($headers); $i++){
						$header = $headers[$i];
						$value = isset($items[$i])? $items[$i] : '';
						$results[strtolower($key)][strtolower($header)] = $value;
					}
					$results[strtolower($key)]['row'] = $key+2;
				}
				return $results;
			} catch(\Exception $e) {
				print('Caught exception 2: '.$e->getMessage()."\n");
				return FALSE;
			}
		} else if($method == 'PUT'){
			try{
				self::setGoogleAPI();
				// Settings
				$optParams = [];
				$optParams['valueInputOption'] = 'RAW';
				// End Settings
				$requestBody = new Google_Service_Sheets_ValueRange();
				$requestBody->setValues([array_values($postData)]);
				// file, range, 
				$response = $this->service->spreadsheets_values->update($this->guest_file, 'Guest Contacts!'.$range, $requestBody, $optParams);
				return $response;
			  } catch(\Exception $e) {
				print('Caught exception 3: '.$e->getMessage()."\n");
				return FALSE;
			  }
		}
  }
  
  /**
	 * Returns an authorized Google API client.
	 * @return Google_Client the authorized client object
  */
	public function getClient() {
	  $client = new Google_Client();
	  $client->setApplicationName($this->googleAppName);
	  $client->setScopes($this->googleScopes);
	  $client->setRedirectUri($this->_redirectURI);
	  $client->setAccessType('offline');
	  $client->setAuthConfig($this->googleSecretFile);
	  $client->setApprovalPrompt('force');

	  // Load previously authorized credentials from a file.
	  $credentialsPath = self::expandHomeDirectory($this->googleCredPath);
	  if (file_exists($credentialsPath)) {
		$accessToken = json_decode(file_get_contents($credentialsPath), true);
	  } else {
		// Request authorization from the user.
		$authUrl = $client->createAuthUrl();
		printf("Open the following link in your browser:\n%s\n", $authUrl);
		print 'Enter verification code: ';
		$authCode = trim(fgets(STDIN));
		// Exchange authorization code for an access token.
		$accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
		// Store the credentials to disk.
		if(!file_exists(dirname($credentialsPath))) {
		  mkdir(dirname($credentialsPath), 0777, true);
		}
		//var_dump($accessToken);
		file_put_contents($credentialsPath, json_encode($accessToken));
		printf("Credentials saved to %s\n", $credentialsPath);
	  }
	  $client->setAccessToken($accessToken);

	  // Refresh the token if it's expired.
	  if($client->isAccessTokenExpired()) {
		$refreshtoken = $client->getRefreshToken();
		$client->fetchAccessTokenWithRefreshToken($refreshtoken);
		file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
	  } else {
		//
	  }
	  return $client;
	}

	/**
	 * Expands the home directory alias '~' to the full path.
	 * @param string $path the path to expand.
	 * @return string the expanded path.
	 */
	public function expandHomeDirectory($path) {
	  $homeDirectory = getenv('HOME');
	  if (empty($homeDirectory)) {
		$homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
	  }
	  return str_replace('~', realpath($homeDirectory), $path);
	}
  
  public function sortByOrder ($array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[]=$array[$ii];
    }
    return $ret;
  }
  
  public function array_find($needle, array $haystack)
  {
    foreach ($haystack as $key => $value) {
        if (false !== stripos($value, $needle)) {
            return $key;
        }
    }
    return false;
  }
  
}
  
?>