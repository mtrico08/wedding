<?php
require_once('constants.php');
require_once('google/vendor/autoload.php');
require_once('google.php');
require_once('session.php'); 

if(isset($_GET)&&count($_GET)>0){
	$roundnum = isset($_GET['round'])? $_GET['round'] : '1';
	$deadline = isset($_GET['deadline'])? str_replace('_', ' ', $_GET['deadline']) : 'the end of next month, please!';
	$ver = isset($_GET['reminder'])? $_GET['reminder'] : '1';
	sendinvite($roundnum,$deadline,$ver);
	return true;
} else {
	return false;
}

// This must accept 1 numerical argument of round #
// It must retrieve all guests from Google API
// It must filter out the ones that match the corresponding round #
// Then it must send the email to the corresponding guest

function sendinvite($roundnum,$deadline,$ver=1){
	$google = new Guests();
	$guests = $google->getGuests();
	$invitelist = '';
	if(!empty($guests)){
		foreach($guests as $guest){
			if($guest['round']==$roundnum&&(!isset($guest['rsvp'])||(isset($guest['rsvp'])&&strlen($guest['rsvp'])==0))){
				/*$invitelist .= $guest['email'].',';
				$mailer = mailer($invitelist,$deadline);*/
				if(isset($guest['email'])&&$guest['email']!==''){
					$mailer = mailer('invited@spaidericottawedding.com',$guest['email'],$deadline,$ver);
					if($mailer == false){
						continue;
					}
				}
			}
			if($ver==3&&$guest['round']==$roundnum&&(!isset($guest['rsvp'])||(isset($guest['rsvp'])&&strlen($guest['rsvp'])!=0))){
				/*$invitelist .= $guest['email'].',';
				$mailer = mailer($invitelist,$deadline);*/
				if(isset($guest['email'])&&$guest['email']!==''){
					$mailer = mailer('invited@spaidericottawedding.com',$guest['email'],$deadline,$ver);
					if($mailer == false){
						continue;
					}
				}
			}
		}
	}
	return false;
}

function mailer($sender, $to,$deadline,$ver=1){
	if($ver==1){
		$subject = 'You\'re invited to Becky Spaide and Mike Ricotta\'s wedding!';
		$body = 'Heck yeah!<br/><br/>You\'re cordially invited to the wedding of Rebecca Ann Spaide and Michael Thomas Ricotta on October 20th, 2018 in Savannah Georgia.<br/>';
		$body .= 'If you\'ve already RSVP\'d, please feel free to ignore this email.<br/><br/>';
		$body .= 'The below link includes the official invitation with all the information that you and your guests will need to know.  We understand the significant efforts involved in traveling to Savannah, over 1,000 miles for many of you, so we understand if you cannot make it but we will also keep this website updated with events and activities in the area to ensure you enjoy your stay.  You may revisit this site any time, to update your information or follow updated timelines on the events!<br/><br/>';
		$body .= 'Please visit <a href="http://www.spaidericottawedding.com">http://www.spaidericottawedding.com</a> or <a href="http://www.alottaspicotta.com">http://www.alottaspicotta.com</a> to RSVP by '.$deadline.'<br/><br/>';
		$body .= 'Thanks!<br/>';
	} else if($ver==2) {
		$subject = 'Reminder, please RSVP to Becky Spaide and Mike Ricotta\'s wedding!';
		$body = 'Heck yeah!<br/><br/>You\'re cordially invited to the wedding of Rebecca Ann Spaide and Michael Thomas Ricotta on October 20th, 2018 in Savannah Georgia.<br/>';
		$body .= 'If you\'ve already RSVP\'d, please feel free to ignore this email.<br/><br/>';
		$body .= 'The below link includes the official invitation with all the information that you and your guests will need to know.  We understand the significant efforts involved in traveling to Savannah, over 1,000 miles for many of you, so we understand if you cannot make it but we will also keep this website updated with events and activities in the area to ensure you enjoy your stay.  You may revisit this site any time, to update your information or follow updated timelines on the events!<br/><br/>';
		$body .= 'Please visit <a href="http://www.spaidericottawedding.com">http://www.spaidericottawedding.com</a> or <a href="http://www.alottaspicotta.com">http://www.alottaspicotta.com</a> to RSVP by '.$deadline.'<br/><br/>';
		$body .= 'Thanks!<br/>';
	} else if($ver==3){
		$subject = 'Friendly reminder of Becky Spaide and Mike Ricotta\'s wedding!';
		$body = "Hello Friends and Family!<br/><br/>";
		$body .= "We are so excited for our upcoming wedding on October 20th!  We can't wait to celebrate our special day with all of you.<br/>";
		$body .= "Thank you so much to everyone who has already submitted your RSVP!  If you haven't received an earlier email, apologies for the confusion (you're invited!).  If you're unable to make it, please accept our apologies for the spam :)<br/>";
		$body .= "<em>If you have not yet responded, or need to change or confirm your attendance, please do so before ".$deadline.".</em><br/><br/>";
		$body .= "You can update/edit your RSVP online through our Website by entering your email or you can contact us via email, text, or phone.<br/>";
		$body .= "<ul><li><strong>Website:</strong>  Spaide & Ricotta Wedding -- Remember to click either 'Will be there!' or 'Cannot make it' next to the name, choose your food preference, and then click 'WooHoo!' to submit your response. This can be edited until ".$deadline."<strong></li><li><strong>Becky:</strong>  rebecca.spaide@yahoo.com or (203) 246-9060<strong></li><li><strong>Mike:</strong>  michael.ricotta@gmail.com or (914) 924-7337<strong></li></ul>";
		$body .= "We strongly suggest booking your flight and hotel reservations early, as October is wedding and festival season in Savannah and will be quite busy. We will be adding new information to the website in the coming weeks, so feel free to check back in for updates. <br/><br/>";
		$body .= "Don't hesitate to contact us with any questions. See you all soon! <br/><br/>";
	}
	$headers = 'From: '.$sender."\r\n".
	  'X-Mailer: PHP/' . phpversion();
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	
	$body .= 'Becky & Mike';
	
	if(false!==mail($to,$subject,$body,$headers)){
		$message = 'All good, everything went through';
                print($message."\r\n");
                print($to."\r\n");		
		return true;
	} else {
		$message = 'Ouchies, we ran into some kind of error.';
		print($message."\r\n");
		print($to."\r\n");
		return false;
	}
}


?>
