<html>
<title>Spaide & Ricotta Wedding | Our Favorite Things</title>
<!-- NOTE, header includes an opened and unclosed DOM element -->
<?php 
	require_once('includes/header.php'); 
?>
	<div id="wrapper" class="">
		<div class="row">
			<div class="container center">
				<h1>Taste of Savannah Party!</h1>
				<h2>Thursday, October 18, 2018 @4PM</h2>
				<h3><em>109 West Liberty St., Savannah GA</em></h3>
				<p>Please join the Spaide family for cocktails and appetizers.  All are welcome.</p>
			</div>
		</div>
		<!--div class="hidden">
			<div class="container center">
				<h1>Rehearsal Dinner</h1>
				<h2>Friday, October 19, 2018 @4PM</h2>
				<h3><em>(Family invitation)</em></h3>
				<p>See email invitation for details.</p>
			</div>
		</div-->
		<div class="row">
			<div class="container center">
				<h1>Wedding!</h1>
				<!--h2>Saturday, October 20, 2018 @4:30PM</h2>
				<h3><em>Family Photos</em></h3-->
				<h2>Ceremony</h2>
				<h3><em>Saturday, October 20, 2018 @5:30PM</em></h3>
				
				<h2>Cocktail Hour</h2>
				<h3><em>Saturday, October 20, 2018 @5:35PM</em></h3>
				
				<h2>Reception</h2>				
				<h3><em>Saturday, October 20, 2018 @6:30PM</em></h3>
				
				<h2>After Party Bar Crawl</h2>
				<h3><em>Saturday, October 20, 2018 @10:00PM</em></h3>
				<p>Route maps will be provided at reception.</p>
			</div>
		</div>
		<div class="row">
			<div class="container center">
				<h1>It's all Over!</h1>
				<h2>Sunday, October 21, 2018</h2>
				<h3><em>Check Out The Favorite Things Page</em></h3>
				<p>No planned events today!  Take a look at our brunch recommendations, you won't be disappointed.  If you have time to travel through our lovely city, check out our recommended sights but don't forget, Savannah has a rich religious history, so many places are closed on Sundays (breweries and bars that don't serve food and pretty much all of mid-town is closed).</p>
			</div>
		</div>
	</div>
<?php require_once('includes/footer.php'); ?>
</html>